# rubocop:disable Metrics/BlockLength
require 'spec_helper'

describe LineContaining do
  it 'has a version number' do
    expect(LineContaining::VERSION).not_to be nil
  end

  it 'does something useful' do
    expect(true).to eq(true)
  end

  describe 'has working functionality' do
    require 'line_containing'
    system('rm -rf tmp')
    system('mkdir tmp')

    it 'before function works' do
      file_w = open('tmp/file1.txt', 'w')
      file_w.write("one two three\n")
      file_w.write("four five six\n")
      file_w.write("ten eleven twelve\n")
      file_w.write('thirteen fourteen fifteen')
      file_w.close
      LineContaining.add_before('ten', 'seven eight nine', 'tmp/file1.txt')
      array_lines = IO.readlines('tmp/file1.txt')
      expect(array_lines[0]).to eq("one two three\n")
      expect(array_lines[1]).to eq("four five six\n")
      expect(array_lines[2]).to eq("seven eight nine\n")
      expect(array_lines[3]).to eq("ten eleven twelve\n")
      expect(array_lines[4]).to eq('thirteen fourteen fifteen')
    end

    it 'after function works' do
      file_w = open('tmp/file2.txt', 'w')
      file_w.write("one two three\n")
      file_w.write("four five six\n")
      file_w.write("ten eleven twelve\n")
      file_w.write('thirteen fourteen fifteen')
      file_w.close
      LineContaining.add_after('six', 'seven eight nine', 'tmp/file2.txt')
      array_lines = IO.readlines('tmp/file2.txt')
      expect(array_lines[0]).to eq("one two three\n")
      expect(array_lines[1]).to eq("four five six\n")
      expect(array_lines[2]).to eq("seven eight nine\n")
      expect(array_lines[3]).to eq("ten eleven twelve\n")
      expect(array_lines[4]).to eq('thirteen fourteen fifteen')
    end

    it 'replace function works' do
      file_w = open('tmp/file3.txt', 'w')
      file_w.write("one two three\n")
      file_w.write("four five six\n")
      file_w.write("sixteen seventeen eighteen\n")
      file_w.write("ten eleven twelve\n")
      file_w.write('thirteen fourteen fifteen')
      file_w.close
      LineContaining.replace('seventeen', 'seven eight nine', 'tmp/file3.txt')
      array_lines = IO.readlines('tmp/file3.txt')
      expect(array_lines[0]).to eq("one two three\n")
      expect(array_lines[1]).to eq("four five six\n")
      expect(array_lines[2]).to eq("seven eight nine\n")
      expect(array_lines[3]).to eq("ten eleven twelve\n")
      expect(array_lines[4]).to eq('thirteen fourteen fifteen')
    end

    it 'delete function works' do
      file_w = open('tmp/file4.txt', 'w')
      file_w.write("one two three\n")
      file_w.write("four five six\n")
      file_w.write("seven eight nine\n")
      file_w.write("sixteen seventeen eighteen\n")
      file_w.write("ten eleven twelve\n")
      file_w.write('thirteen fourteen fifteen')
      file_w.close
      LineContaining.delete('seventeen', 'tmp/file4.txt')
      array_lines = IO.readlines('tmp/file4.txt')
      expect(array_lines[0]).to eq("one two three\n")
      expect(array_lines[1]).to eq("four five six\n")
      expect(array_lines[2]).to eq("seven eight nine\n")
      expect(array_lines[3]).to eq("ten eleven twelve\n")
      expect(array_lines[4]).to eq('thirteen fourteen fifteen')
    end

    it 'before function works at start of file' do
      file_w = open('tmp/file5.txt', 'w')
      file_w.write("four five six\n")
      file_w.write("seven eight nine\n")
      file_w.close
      LineContaining.add_before('four', 'one two three', 'tmp/file5.txt')
      array_lines = IO.readlines('tmp/file5.txt')
      expect(array_lines[0]).to eq("one two three\n")
      expect(array_lines[1]).to eq("four five six\n")
      expect(array_lines[2]).to eq("seven eight nine\n")
    end

    it 'after function works at end of file' do
      file_w = open('tmp/file6.txt', 'w')
      file_w.write("one two three\n")
      file_w.write('four five six')
      file_w.close
      LineContaining.add_after('six', 'seven eight nine', 'tmp/file6.txt')
      array_lines = IO.readlines('tmp/file6.txt')
      expect(array_lines[0]).to eq("one two three\n")
      expect(array_lines[1]).to eq("four five six\n")
      expect(array_lines[2]).to eq("seven eight nine\n")
    end

    it 'delete between function works' do
      file_w = open('tmp/file7.txt', 'w')
      file_w.write("one two three\n")
      file_w.write("a b c\n")
      file_w.write("d e f\n")
      file_w.write("g h i\n")
      file_w.write("four five six\n")
      file_w.close
      LineContaining.delete_between('one', 'six', 'tmp/file7.txt')
      array_lines = IO.readlines('tmp/file7.txt')
      expect(array_lines[0]).to eq("one two three\n")
      expect(array_lines[1]).to eq("four five six\n")
    end

    it 'delete between plus function works' do
      file_w = open('tmp/file8.txt', 'w')
      file_w.write("one two three\n")
      file_w.write("a b c\n")
      file_w.write("d e f\n")
      file_w.write("g h i\n")
      file_w.write("four five six\n")
      file_w.close
      LineContaining.delete_between_plus('a b', 'h i', 'tmp/file8.txt')
      array_lines = IO.readlines('tmp/file8.txt')
      expect(array_lines[0]).to eq("one two three\n")
      expect(array_lines[1]).to eq("four five six\n")
    end
  end
end
# rubocop:enable Metrics/BlockLength
