require 'line_containing/version'

#
module LineContaining
  def self.add_before(str_orig, str_add, path)
    path_old = path
    path_new = "#{path_old}.new"
    file_w = File.open(path_new, 'w')
    File.readlines(path_old).each do |line|
      file_w.write("#{str_add}\n") if line.include? str_orig
      file_w.write(line)
    end
    file_w.close
    system("rm #{path_old}")
    system("mv #{path_new} #{path_old}")
  end

  def self.add_after(str_orig, str_add, path)
    path_old = path
    path_new = "#{path_old}.new"
    file_w = File.open(path_new, 'w')
    File.readlines(path_old).each do |line|
      file_w.write(line)
      if line.include? str_orig
        file_w.write("\n") if line[-1] != "\n"
        file_w.write("#{str_add}\n")
      end
    end
    file_w.close
    system("rm #{path_old}")
    system("mv #{path_new} #{path_old}")
  end

  def self.replace(str_orig, str_new, path)
    path_old = path
    path_new = "#{path_old}.new"
    file_w = File.open(path_new, 'w')
    File.readlines(path_old).each do |line|
      if line.include? str_orig
        file_w.write("#{str_new}\n")
      else
        file_w.write(line)
      end
    end
    file_w.close
    system("rm #{path_old}")
    system("mv #{path_new} #{path_old}")
  end

  def self.delete(str_orig, path)
    path_old = path
    path_new = "#{path_old}.new"
    file_w = File.open(path_new, 'w')
    File.readlines(path_old).each do |line|
      if line.include? str_orig
        # Print NOTHING
      else
        file_w.write(line)
      end
    end
    file_w.close
    system("rm #{path_old}")
    system("mv #{path_new} #{path_old}")
  end

  def self.delete_between(str1, str2, path)
    path_old = path
    path_new = "#{path_old}.new"
    file_w = File.open(path_new, 'w')
    to_delete = false
    File.readlines(path_old).each do |line|
      if line.include? str1
        file_w.write(line)
        to_delete = true
      elsif line.include? str2
        file_w.write(line)
        to_delete = false
      elsif to_delete == false
        file_w.write(line)
      end
    end
    file_w.close
    system("rm #{path_old}")
    system("mv #{path_new} #{path_old}")
  end

  def self.delete_between_plus(str1, str2, path)
    delete_between(str1, str2, path)
    delete(str1, path)
    delete(str2, path)
  end
end
