# LineContaining

[![Gem Version](https://badge.fury.io/rb/line_containing.svg)](https://badge.fury.io/rb/line_containing)
[![Build Status](https://semaphoreci.com/api/v1/jhsu802701/line_containing/branches/master/badge.svg)](https://semaphoreci.com/jhsu802701/line_containing)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/876cf0d94d3541c18e903b0ed4ce574c)](https://www.codacy.com/app/jhsu802701/line_containing?utm_source=jhsu802701@bitbucket.org&amp;utm_medium=referral&amp;utm_content=jhsu802701/line_containing&amp;utm_campaign=Badge_Grade)
[![codebeat badge](https://codebeat.co/badges/2f5046f6-57ed-448f-8d01-c9942c26c399)](https://codebeat.co/projects/bitbucket-org-jhsu802701-line_containing-master)
[![codecov](https://codecov.io/bb/jhsu802701/line_containing/branch/master/graph/badge.svg)](https://codecov.io/bb/jhsu802701/line_containing)

## What This Gem Does

The LineContaing gem is changing the contents of a file based on the content in a line.  Some examples:

* Look for a line containing specific content, and add a new line before it.
```
one two three
four five six
ten eleven twelve
thirteen fourteen fifteen
```
Add the line "seven eight nine" before the line containing "ten".
```
one two three
four five six
seven eight nine
ten eleven twelve
thirteen fourteen fifteen
```

* Look for a line containing specific content, and add a new line after it.

```
one two three
four five six
ten eleven twelve
thirteen fourteen fifteen
```
Add the line "seven eight nine" after the line containing "six"
```
one two three
four five six
seven eight nine
ten eleven twelve
thirteen fourteen fifteen
```

* Look for a line containing specific content, and add a new line in place of it.

```
one two three
four five six
sixteen seventeen eighteen
ten eleven twelve
thirteen fourteen fifteen
```

Add the line "seven eight nine" in place of the line containing "seventeen".

```
one two three
four five six
seven eight nine
ten eleven twelve
thirteen fourteen fifteen
```

* Look for a line containing specific content, and delete this line.

```
one two three
four five six
seven eight nine
sixteen seventeen eighteen
ten eleven twelve
thirteen fourteen fifteen
```

Delete the line containing "seventeen".

      
```
one two three
four five six
seven eight nine
ten eleven twelve
thirteen fourteen fifteen
```

* Look for lines containing specific content, and delete everything in between.
```
one two three
a b c
d e f
g h i
four five six
```
Delete the lines between "two" and "five".
```
one two three
four five six
```

* Look for lines containing specific content, delete them, and delete everything in between.
```
one two three
a b c
d e f
g h i
four five six
```
Delete the lines containing "two" and "five" plus everything in between.
```
one two three
four five six
```

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'line_containing'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install line_containing

## Usage

#### Look for a line containing specific content, and add a new line before it.
EXAMPLE: In the file "file1.txt", look for the line containing "ten", and add the line "seven eight nine" before it.

```
LineContaining.add_before("ten", "seven eight nine", "file1.txt")
```

#### Look for a line containing specific content, and add a new line after it.
EXAMPLE: In the file "file2.txt", look for the line containing "six", and add the line "seven eight nine" after it.
```
LineContaining.add_after("six", "seven eight nine", "file2.txt")
```

#### Look for a line containing specific content, and add a new line in place of it.
EXAMPLE: In the file "file3.txt", look for the line containing "seventeen", and add the line "seven eight nine" in place of it.
```
LineContaining.replace("seventeen", "seven eight nine", "file3.txt")
```

#### Look for a line containing specific content, and delete this line.
EXAMPLE: In the file "file4.txt", look for the line containing "seventeen", and delete it.
```
LineContaining.delete("seventeen", "file4.txt")
```

#### Look for lines containing specific content, and delete everything in between.
EXAMPLE: In the file "file7.txt", look for the line containing "one" and the line containing "six", and delete everything in between.
```
LineContaining.delete_between('one', 'six', 'file7.txt')
```

#### Look for lines containing specific content.  Delete these lines and everything in between.
EXAMPLE: In the file "file8.txt", look for the line containing "a b" and the line containing "h i".  Delete these lines and everything in between.
```
LineContaining.delete_between_plus('a b', 'h i', 'file8.txt')
```

## Development


### Testing this gem

Enter `sh gem_test.sh`.

### Running this gem in irb

Enter `sh gem_console.sh`.

### Installing this gem

Enter `sh gem_install.sh`.

## Contributing

1. Fork it ( https://github.com/[my-github-username]/line_containing/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
