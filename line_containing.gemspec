lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'line_containing/version'

Gem::Specification.new do |spec|
  spec.name          = 'line_containing'
  spec.version       = LineContaining::VERSION
  spec.authors       = ['Jason Hsu']
  spec.email         = ['rubyist@jasonhsu.com']

  spec.summary       = 'Remove or replace a line from a file based on its content.  Look for a line with certain content, and add another line before or after it.'
  spec.description   = 'Remove or replace a line from a file based on its content.  Look for a line with certain content, and add another line before or after it.'
  spec.homepage      = 'https://bitbucket.org/jhsu802701/line_containing'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '1.16.3'
  spec.add_development_dependency 'bundler-audit', '0.6.0'
  spec.add_development_dependency 'codecov', '0.1.10'
  spec.add_development_dependency 'gemsurance', '0.9.0'
  spec.add_development_dependency 'rake', '12.3.1'
  spec.add_development_dependency 'rspec', '3.8.0'
  spec.add_development_dependency 'rubocop', '0.58.2'
  spec.add_development_dependency 'ruby-graphviz', '1.2.3'
  spec.add_development_dependency 'sandi_meter', '1.2.0'
  spec.add_development_dependency 'simplecov', '0.16.1'
end
